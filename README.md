# PHP SSO Example APP 

This package includes an index.php which already contains the lines of code to get SSO working. 

In order to get the SSO client you must have Composer installed on your system, once that's done simply open up the terminal in the directory where the index.php and composer.json exist and input the following command:

```
Composer install
```

Once it finishes downloading, index.php will now be behind EWU's SSO.