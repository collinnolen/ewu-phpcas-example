<?php

#THESE LINES OF CODE BELOW IS REQUIRED
require './vendor/autoload.php';

$sso = new EWU\SingleSignOn\SSOClient();

$sso->authenticate();
#END OF REQUIRED CODE

#You can get credentials when authenticating by doing the following
#$results = $sso->authenticate();
#Or you can get the credentials after authentication by doing the following
#$results = $sso->credentials();
#$results is an associative array which contains two parts. $results['username'] and $results['attributes']


# This is another way to get the user's NetID
$username = $sso->get_user();

# Or you can do this, assuming you have a variable named $results which holds user credentials
#$username = $results['username'];
echo "Hello , ".$username.", you are authenticated!";

# If you want additional attributes for the authenticated user, you can either
# call this function
$attributes = $sso->get_attributes();

# Or you can use PHP's associative arrays, assuming you have an array $results holding user credentials
#$attributes = $results['attributes'];

# Or you can use (assuming you have $results holding user credentials) the associative array itself
# i.e dosomething($results['attributes']);
# However, we recommend using the first method of obtaining user attributes

# If you want to know which attributes are available, you can use the following (uncomment)
/*
ksort($attributes);
foreach ($attributes as $key => $value) {
    if (is_array($value)) {
        echo '<li><strong>', $key, ':<ul>';
        foreach ($value as $item) {
            echo '<li></strong>', $item, '</li>';
        }
        echo '</ul></li>';
    } else {
        echo '<li><strong>', $key, ': </strong>', $value, '</li>'.PHP_EOL;
    }
}
*/
?>
